package com.example.demo.controller;
import com.example.demo.repository.UserRepository;
import com.example.demo.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;



@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/signin")
    public String signinPage() {
        return "redirect:/signin.html";
    }

    @PostMapping("/signin")
    public String signinUser(HttpServletRequest request,
                            @RequestParam String username, @RequestParam String password) {
        User user = userRepository.findByUsername(username);
        if (user != null && user.getPassword().equals(password)) {
            HttpSession session = request.getSession();
            session.setAttribute("userLoggedIn", true);
            session.setAttribute("userUsername", user.getUsername());
            int oneDayInSeconds =  15 * 60;
            session.setMaxInactiveInterval(oneDayInSeconds);

            return "redirect:/signin-success.html";
        } else {
            return "redirect:/signin-fail.html";
        }
    }

    @GetMapping("/signup")
    public String registerPage() {
        return "redirect:/signup.html";
    }

    @PostMapping("/signup")
    public String signinUser(@RequestParam String type, @RequestParam String username, @RequestParam String email, @RequestParam String password) {
        if (userRepository.existsByUsername(username)) {
            return "redirect:/signup-fail.html";
        } else {
            User user = new User();
            user.setUsername(username);
            user.setEmail(email);
            user.setPassword(password);
            user.setType(type);
            userRepository.save(user);
            return "redirect:/signup-success.html";
        }
    }

    @GetMapping("/signout")
    public String signout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return "redirect:/index.html";
    }

    @GetMapping("/edit-user")
    public String editUser(HttpServletRequest request , @RequestParam String username){
        HttpSession session = request.getSession(true);
        User user = userRepository.findByUsername(username);
        if (session == null && user == null){
            return "redirect:/edit-user-fail.html";
        }
        return "redirect:/edit-user-success.html";
    }

    @PostMapping("/edit-user")
    public String updateUser(HttpServletRequest request, @RequestParam String name
                            , @RequestParam String email
                            , @RequestParam String password){
        HttpSession session = request.getSession();
        String userUsername = (String) session.getAttribute("userUsername");
        User user = userRepository.findByUsername(userUsername);
        if (user == null){
            user = new User();
            user.setUser(user);
        }
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            userRepository.save(user);

        return "redirect:/user-profile.html";
    }

    @GetMapping("/user-profile-data")
    @ResponseBody
    public ResponseEntity<User> viewUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String userUsername = (String) session.getAttribute("userUsername");

        if (userUsername != null) {
            User user = userRepository.findByUsername(userUsername);
            if (user != null) {
                return ResponseEntity.ok(user);
            }
        }

        return ResponseEntity.notFound().build();
    }
}


