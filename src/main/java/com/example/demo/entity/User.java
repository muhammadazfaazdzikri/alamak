package com.example.demo.entity;

import jakarta.persistence.*;


@Entity
@Table(name = "user_db", schema = "public")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
    private String username;
    private String name;
    private String email;
    private String password;
    private String type;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setUsername(String username) { this.username = username;}
    public void setEmail(String email) { this.email = email;}

    public String getEmail(){return email;}
    public Long getUser_id() {return user_id;}

    public String getUsername() {return username;}
    public void setType(String type) {this.type = type;}
    public String getType(){return type;}
    public String getName(){return name;}
    public void setName(String name) {this.name = name;}
    public void setUser(User user) {this.user = user;}
    public User getUser(){return user;}

}

